import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

// yarn add eslint -D
// yarn eslint --init
// yarn add prettier eslint-config-prettier eslint-plugin-prettier babel-eslint  -D
// yarn add react-router-dom
// yarn add styled-components
// yarn add react-icons
// trabalhar com cores
// yarn add polished
// yarn add json-server -D
// yarn add axios
// yarn add redux react-redux
// yarn add reactotron-react-js reactotron-redux
// trabalhar com arrays
// yarn add immer
// yarn add redux-saga
// yarn add  reactotron-redux-saga
// yarn add react-toastify
// yarn add history
ReactDOM.render(<App />, document.getElementById('root'));
