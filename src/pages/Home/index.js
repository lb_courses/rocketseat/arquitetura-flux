import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { MdAddShoppingCart } from 'react-icons/md';
import { ProductList } from './styles';
import { formatPrice } from '../../utils/format';
import api from '../../services/api';
import * as CartActions from '../../store/modules/cart/actions';

class Home extends Component {
  // export default class Home extends Component {
  state = {
    products: [],
  };

  async componentDidMount() {
    const response = await api.get('products');

    // executa a formatação assim que pega os dados da api
    // para executar apenas uma unica vez
    const data = response.data.map(product => ({
      ...product,
      priceFormatted: formatPrice(product.price),
    }));

    this.setState({ products: data });
  }

  // handleAddProduct = product => {
  handleAddProduct = id => {
    // só tem acesso ao dispatch quando usa redux
    // dispatch({
    // const { dispatch } = this.props;
    //   type: 'ADD_TO_CART',
    //   product,
    // });

    // const { dispatch } = this.props;
    // dispatch(CartActions.addToCart(product));

    const { addToCartRequest } = this.props;
    addToCartRequest(id);
    // a navegação pode acontecer antes do retorno da função do saga
    // por isso a navegação e feita dentro do próprio saga
    // this.props.history.push('/cart');
  };

  render() {
    const { products } = this.state;
    const { amount } = this.props;
    return (
      <ProductList>
        {products.map(product => (
          <li key={product.id}>
            <img src={product.image} alt={product.title} />
            <strong>{product.title}</strong>
            <span>{product.priceFormatted}</span>
            {/* <span>{formatPrice(product.price)}</span> */}

            <button
              type="button"
              onClick={() => this.handleAddProduct(product.id)}
            >
              <div>
                <MdAddShoppingCart size={16} color="#FFF" />{' '}
                {amount[product.id] || 0}
              </div>

              <span>ADICIONAR AO CARRINHO</span>
            </button>
          </li>
        ))}
      </ProductList>
    );
  }
}

const mapStateToProps = state => ({
  amount: state.cart.reduce((amount, product) => {
    // a chave e o id e o valor o amount
    amount[product.id] = product.amount;
    return amount;
  }, {}),
});

// converte as actions do redux em propriedades do component
const mapDispatchToProps = dispatch =>
  bindActionCreators(CartActions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);
